using System;
using System.Text;

public class CompressString
{
    // Compress string by counting consecutive letters
    // aaaaaaabbbbccccc -> a7b4c5
    static public void Main(string[] args)
    {
        StringBuilder buffer = new StringBuilder();
        string input = args[0];
        int count = 1;
        char prev = input[0];

        for(int i=1; i<input.Length; i++)
        {
            if(input[i] == prev)
                count++;
            else
            {
                buffer.Append(prev);
                buffer.Append(count.ToString());
                prev = input[i];
                count = 1;
            }
        }

        buffer.Append(prev);
        buffer.Append(count.ToString());
        string candidate = buffer.ToString();
        string output = candidate.Length < input.Length ? candidate : input;

        Console.WriteLine("input string: " + input);
        Console.WriteLine("compressed: " + output);
    }
}
