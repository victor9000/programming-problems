using System;
using System.Text;
 
public class CopyLinkedList
{
    static public void Main(string[] args)
    {
        // create nodes
        LinkedListNode<int> node_d = new LinkedListNode<int>(4);
        LinkedListNode<int> node_c = new LinkedListNode<int>(3, node_d);
        LinkedListNode<int> node_b = new LinkedListNode<int>(2, node_c);
        LinkedListNode<int> node_a = new LinkedListNode<int>(1, node_b);

        PrintList(node_a);
        LinkedListNode<int> copy = CopyList(node_a);
        PrintList(copy);
    }

    static public void PrintList(LinkedListNode<int> head) {
        LinkedListNode<int> temp = head;
        string list = "";

        while(temp != null) {
            list += temp.Data + ", ";
            temp = temp.Next;
        }

        Console.WriteLine(list.Substring(0, list.Length - 2));
    }

    static public LinkedListNode<int> CopyList(LinkedListNode<int> head) {
        if(head == null)
            return null;

        // points to first node
        LinkedListNode<int> copy_head = new LinkedListNode<int>();
        copy_head.Data = head.Data;

        // tail node acts as a runner for the copy list
        LinkedListNode<int> copy_tail = copy_head;

        // current acts as a runner for the source list
        LinkedListNode<int> current = head.Next;
        
        while(current != null) {
            copy_tail.Next = new LinkedListNode<int>();
            copy_tail.Next.Data = current.Data;
            copy_tail = copy_tail.Next;

            current = current.Next;
        }

        return copy_head;
    }
}
