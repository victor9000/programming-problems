using System;
using System.Text;
 
public class GetCycleStartLinkedList
{
    static public void Main(string[] args)
    {
        // create nodes
        LinkedListNode<int> A = new LinkedListNode<int>(1);
        LinkedListNode<int> B = new LinkedListNode<int>(2);
        LinkedListNode<int> C = new LinkedListNode<int>(3);
        LinkedListNode<int> D = new LinkedListNode<int>(4);
        LinkedListNode<int> E = new LinkedListNode<int>(5);
        LinkedListNode<int> F = new LinkedListNode<int>(6);
        LinkedListNode<int> G = new LinkedListNode<int>(7);
        LinkedListNode<int> H = new LinkedListNode<int>(8);
        LinkedListNode<int> I = new LinkedListNode<int>(9);
        LinkedListNode<int> J = new LinkedListNode<int>(10);
        LinkedListNode<int> K = new LinkedListNode<int>(11);

        // create a linked list with a cycle at D:
        // A->B->C->D->E->F->G->H->I->J->K->D
        A.Next = B;
        B.Next = C;
        C.Next = D;
        D.Next = E;
        E.Next = F;
        F.Next = G;
        G.Next = H;
        H.Next = I;
        I.Next = J;
        J.Next = K;
        K.Next = D;
       
        LinkedListNode<int> cycle_node = GetCycleStartNode(A);

        // should be 4
        Console.WriteLine(cycle_node == null ? -1 : cycle_node.Data); 
    }

    static public LinkedListNode<int> GetCycleStartNode(LinkedListNode<int> head) {
        LinkedListNode<int> intersect_node = null;
        LinkedListNode<int> cycle_node = null;
        LinkedListNode<int> slow_node = null;
        LinkedListNode<int> fast_node = null;
        bool is_search = true;

        if(head == null || head.Next == null)
            return null;

        // create a slow node and a fast node.
        // the fast node will travel at 2x the speed of the slow node
        // if there's a cycle then the two nodes will intersect at an
        // intersection node.  the intersection node will be as 
        // far from the start of the cycle as the head node.
        slow_node = head;
        fast_node = head;
       
        // search until we identify the interesction node
        while(is_search) {
            slow_node = slow_node.Next;
            fast_node = fast_node.Next.Next;

            if(fast_node == null || fast_node.Next == null)
                is_search = false;
            else if (slow_node == fast_node)
                intersect_node = slow_node;
           
            is_search = is_search && intersect_node == null;
        }

        // if we found an intersection node then advance the
        // head and the intersection node until they are the same.
        // that will be the start of the cycle.
        if (intersect_node != null) {
            while(intersect_node != head) {
                intersect_node = intersect_node.Next;
                head = head.Next;
            }
            cycle_node = intersect_node;
        }

        return cycle_node;
    }
}
