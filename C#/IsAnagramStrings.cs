using System;
using System.Linq;
 
public class IsAnagramString
{
    // Is string1 an anagram of string2?
    static public void Main(string[] args)
    {
        string token1 = args[0];
        string token2 = args[1];
        
        string ordered1 = new string(token1.OrderBy(c => c).ToArray());
        string ordered2 = new string(token2.OrderBy(c => c).ToArray());

        Console.WriteLine("Is string1 and anagram of string2?");
        Console.WriteLine(ordered1 == ordered2);
    }
}
