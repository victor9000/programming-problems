using System;
 
public class IsUniqueChars
{
    // determine if the input string is composed of unique characters
    static public void Main(string[] args)
    {
        bool isUnique = true;
        string token = args[0];
        bool[] cache = new bool[100];

        for(int i=0; i < token.Length && isUnique; i++) {
            int key = (int)token[i] - 32;

            if (cache[key] == false)
                cache[key] = true;
            else
                isUnique = false;
        }

        string fmt_output = "Is '{0}' composed of unique chars?";
        Console.WriteLine(string.Format(fmt_output, token));
        Console.WriteLine(isUnique.ToString());
    }
}
