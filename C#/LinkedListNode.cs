using System;
using System.Text;
 
public class LinkedListNode<T>
{
    public T Data = default(T);
    public LinkedListNode<T> Next = null; 

	public LinkedListNode() { }

    public LinkedListNode(T data) {
        this.Data = data;
    }  

    public LinkedListNode(T data, LinkedListNode<T> next) {
        this.Data = data;
        this.Next = next;
    }  
}
