using System;
using System.Linq;
 
public class ReplaceStringImmutable
{
    // Replace part a of string with a new string value
    static public void Main(string[] args)
    {
        string blob = args[0];
        string old_part = args[1];
        string new_part = args[2];
        int index = -1;

        index = blob.IndexOf(old_part);
        while (index > -1) {
            string left = blob.Substring(0, index);
            string right = blob.Substring(index + old_part.Length);
            blob = left + new_part + right;
            index = blob.IndexOf(old_part);
        }

        Console.WriteLine("old string: " + args[0]);
        Console.WriteLine("new string: " + blob);
    }
}
