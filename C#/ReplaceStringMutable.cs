using System;
using System.Text;

public class ReplaceStringMutable
{
    // Replace part a of string with a new string using a string builder
    static public void Main(string[] args)
    {
        string blob = args[0];
        string old_part = args[1];
        string new_part = args[2];
        StringBuilder builder = new StringBuilder();

        for (int i=0; i<blob.Length; i++)
        {
            bool is_match = true;
            for (int j=0; j<old_part.Length && is_match; j++)
            {
                if (i + j > blob.Length - 1)
                    is_match = false;
                else
                    is_match = blob[i+j] == old_part[j];
            }

            if (!is_match)
                builder.Append(blob[i]);
            else
            {
                builder.Append(new_part);
                i += old_part.Length - 1;
            }
        }

        Console.WriteLine("old string: " + blob);
        Console.WriteLine("new string: " + builder.ToString());
    }
}
