using System;
using System.Text;
 
public class ReverseString
{
    // reverse a string
    static public void Main(string[] args)
    {
        string token = args[0];
        StringBuilder builder = new StringBuilder(token);

        int swaps = (int)(builder.Length / 2);
        for(int i = 0; i < swaps; i++) {
            char temp = builder[i];
            builder[i] = builder[builder.Length - i - 1];
            builder[builder.Length - i - 1] = temp; 
        }

        Console.WriteLine("original: " + token);
        Console.WriteLine("reverse: " + builder.ToString());
    }
}
