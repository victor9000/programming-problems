using System;
using System.Text;
 
public class ReverseStringXOR
{
    // reverse a string using the exclusive-or bitwise operator
    static public void Main(string[] args)
    {
        string token = args[0];
        StringBuilder builder = new StringBuilder(token);
        int swaps = (int)(builder.Length / 2);

        for(int i = 0; i < swaps; i++) {
            int l_index = i;
            int r_index = builder.Length - i - 1;

            builder[l_index] = (char)(builder[i] ^ builder[r_index]);
            builder[r_index] = (char)(builder[i] ^ builder[r_index]);
            builder[l_index] = (char)(builder[i] ^ builder[r_index]); 
        }

        Console.WriteLine("original: " + token);
        Console.WriteLine("xor reverse: " + builder.ToString());
    }
}
