using System;
 
public class TowersOfHanoi
{
    static public void Main(string[] args)
    {
        int num_disks = int.Parse(args[0]);
        make_move(num_disks, 'A', 'C', 'B');
    }

    static public void make_move(int num_disks, char source, char destination, char via) {
        if (num_disks == 0)
            return;

        make_move(num_disks - 1, source, via, destination);
        Console.WriteLine("move " + source + " to " + destination);
        make_move(num_disks - 1, via, destination, source);
    }
}
