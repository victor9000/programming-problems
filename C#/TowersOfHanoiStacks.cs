using System;
using System.Collections.Generic;

public class TowersOfHanoi
{
    static public void Main(string[] args)
    {
        int num_disks = int.Parse(args[0]);
        var A =  new { Name = "Stack-A", Data = new Stack<int>() };
        var B =  new { Name = "Stack-B", Data = new Stack<int>() };
        var C =  new { Name = "Stack-C", Data = new Stack<int>() };

        for (int i = num_disks; i > 0; i--)
            A.Data.Push(i);

        Console.WriteLine("Initial State:");
        print(A);
        print(B);
        print(C);
        Console.WriteLine("");

        Console.WriteLine("Operations:");
        make_move(num_disks, A, C, B);
        Console.WriteLine("");

        Console.WriteLine("Final State:");
        print(A);
        print(B);
        print(C);
        Console.WriteLine("");
    }

    static public void make_move(int num_disks, dynamic source, dynamic destination, dynamic via) {
        if (num_disks == 0)
            return;

        make_move(num_disks - 1, source, via, destination);

        int disk = source.Data.Pop();
        destination.Data.Push(disk);
        Console.WriteLine(String.Format(
            "Move disk {0} from {1} to {2}", disk.ToString(), source.Name, destination.Name
        ));

        make_move(num_disks - 1, via, destination, source);
    }

    static public void print(dynamic stack) {
        Console.WriteLine(stack.Name);

        if (stack.Data.Count == 0)
            Console.WriteLine("Empty");
        else 
        {
            int[] arr_data = stack.Data.ToArray();
            string message = "";
            for (int i=0; i < arr_data.Length; i++)
                message += arr_data[i] + ", ";

            Console.WriteLine(message.Substring(0, message.Length - 2));
        }
    }
}
