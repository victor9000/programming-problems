using System;
 
public class TreeNode<T>
{
    public T Data = default(T);
    public TreeNode<T> Left = null;
    public TreeNode<T> Right = null; 

    public TreeNode(T data) {
        this.Data = data;
    }

    public TreeNode(T data, TreeNode<T> left, TreeNode<T> right) {
        this.Data = data;
        this.Left = left;
        this.Right = right;
    }  
}
