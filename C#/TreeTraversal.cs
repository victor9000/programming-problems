using System;

public class TreeTraversal
{
    static public void Main(string[] args)
    {
        TreeNode<int> root = new TreeNode<int>(10);
        root.Left = new TreeNode<int>(5);
        root.Left.Left = new TreeNode<int>(3);
        root.Left.Right = new TreeNode<int>(7);
        root.Right = new TreeNode<int>(15);
        root.Right.Left = new TreeNode<int>(13);
        root.Right.Right = new TreeNode<int>(17);

        string order = args[0];
        traverse(root, order);
    }

    // traverse tree
    static public void traverse(TreeNode<int> node, string order) {
        if (node == null)
            return;

        if(order == "pre-order")
            Console.WriteLine(node.Data);

        traverse(node.Left, order);

        if(order == "in-order")
            Console.WriteLine(node.Data);

        traverse(node.Right, order);

        if(order == "post-order")
            Console.WriteLine(node.Data);
    }
}
