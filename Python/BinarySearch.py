def binarysearch(items, target):
    low = 0
    high = len(items) - 1
    mid = (low + high) / 2

    while low <= high:
        if items[mid] == target:
            return True
        elif target > items[mid]:
            low = mid + 1
        else:
            high = mid - 1

        mid = (low + high) / 2

    return False

items = range(-10, 10, 2)
print items

for i in range(-15, 15, 1):
    print i, binarysearch(items, i)
