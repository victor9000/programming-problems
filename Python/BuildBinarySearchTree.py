class Node:
    def __init__(self, value=None, left=None, right=None):
        self._value = value
        self._left = left
        self._right = right

    def __repr__(self):
        return str(self._value)

def make_tree(array, n_root, n_left, n_right):
    node = Node(array[n_root])

    left_left = n_left
    left_right = n_root - 1
    left_root = left_left + int((left_right - left_left) / 2)

    if (left_left <= left_root):
        node._left = make_tree(array, left_root, left_left, left_right)

    right_left = n_root + 1
    right_right = n_right
    right_root = right_left + int((right_right - right_left) / 2)
    if (right_left <= right_root):
        node._right = make_tree(array, right_root, right_left, right_right)

    return node

def print_tree(node, pad):
    print ''.rjust(pad) + str(node)
    pad = pad + 4
    if node._left is not None:
        print_tree(node._left, pad)

    if node._right is not None:
        print_tree(node._right, pad)

tree_array = [1,5,7,11,21,27,33,45,51,57,62,65,71,78,83]
index_root = len(tree_array) / 2
index_left = 0
index_right = len(tree_array) - 1

root = make_tree(tree_array, index_root, index_left, index_right)
print_tree(root, 0)
