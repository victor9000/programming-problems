#
class Node:
    def __init__(self, value=None, next=None):
        self.value = value
        self.next = next

    def __str__(self):
        return str(self.value) + ('' if self.next is None else str(self.next))

#
def parse(equation):
    node=Node('=')
    root=node
    num=''

    for c in equation:
        if c not in ['*','/','+','-']:
            num += c
        else:
            node.next = Node(int(num))
            node = node.next

            node.next = Node(c)
            node = node.next
            num = ''

    node.next = Node(int(num))
    node.next.next = Node('#')

    return root

#
def operation(lhs, op, rhs):
    val = 0

    if op.value == '*':
        val = float(lhs.value) * float(rhs.value)
    elif op.value == '/':
        val = float(lhs.value) / float(rhs.value)
    elif op.value == '+':
        val = float(lhs.value) + float(rhs.value)
    elif op.value == '-':
        val = float(lhs.value) - float(rhs.value)

    return val

#
def reduce(root, priority):
    target_ops=None
    if priority == True:
        target_ops=['*', '/']
    else:
        target_ops=['+', '-']

    prev = root
    lhs = prev.next
    op = lhs.next
    rhs = op.next

    while op.value is not '#':
        if op.value in target_ops:
            val = operation(lhs, op, rhs)
            new = Node(val)
            prev.next = new
            new.next = rhs.next
        else:
            prev = prev.next

        lhs = prev.next
        op = lhs.next
        rhs = op.next

    return root

#
def calc(node):
    reduced = reduce(eq, True)
    final = reduce(reduced, False)
    return final.next.value

string = '25+10/2-5*3+1/2'
eq = parse(string)
print calc(eq)
