
def fib_memo(n, cache):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        f1 = cache[n-1] if n-1 in cache else fib_memo(n-1, cache)
        cache[n-1] = f1
        f2 = cache[n-2] if n-2 in cache else fib_memo(n-2, cache)
        cache[n-2] = f2
        
        return  f1 + f2
        
    
def fib_flat(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    
    prev2 = 0
    prev1 = 1
    fib = 0
    for i in range(n-1):
        fib = prev2 + prev1
        prev2 = prev1
        prev1 = fib
        
    return fib
    
    
print fib_flat(200)
print fib_memo(200, {})
