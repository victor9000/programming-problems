
def find_subsets(current_set, all_sets=[], count=[0]):
    if not current_set or current_set in all_sets:
        return

    all_sets.append(current_set)

    for i in range(0, len(current_set)):
        find_subsets(current_set[:i] + current_set[i+1:], all_sets)


if __name__ == "__main__":
    import sys
    num = int(sys.argv[1])
    subsets = []
    find_subsets([i+1 for i in range(num)], subsets)

    import pprint
    pprint.pprint(subsets)
    print len(subsets)
