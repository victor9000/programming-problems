import math

def find_subsets(source):
    exp = len(source)
    max_num = int(math.pow(2, exp))
    fmt = '0{0}b'.format(str(exp))
    sets = []

    for i in range(max_num):
        bits = [int(b) for b in format(i, fmt)]
        filtered = filter(lambda x: x[0] != 0, zip(bits, source))
        sets.append([f[1] for f in filtered])

    return sets

if __name__ == "__main__":
    import pprint, sys
    num = 12
    full_set = [chr(i) for i in range(97, 97+num)]
    all_sets = find_subsets(full_set)
    pprint.pprint(all_sets)
    print len(all_sets)
