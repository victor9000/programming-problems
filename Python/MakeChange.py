import sys

def make_change(total, cache=[], dollars=0, quarters=0, dimes=0, nickels=0, pennies=0):
    key_fmt = "{0}.{1}.{2}.{3}.{4}"
    key = key_fmt.format(dollars, quarters, dimes, nickels, pennies)
    if key in cache:
        return
    else:
        cache.append(key)

    if total < 0:
        return
    elif total == 0:
        fmt = "{0} dollars, {1} quarters, {2} dimes, {3} nickels, {4} pennies"
        print fmt.format(dollars, quarters, dimes, nickels, pennies)
        return

    _dollars = total / 100
    _quarters = total / 25
    _dimes = total / 10
    _nickles = total / 5
    _pennies = total

    if _dollars > 0:

        make_change(total-100, cache, dollars+1, quarters, dimes, nickels, pennies)

    if _quarters > 0:

        make_change(total-25, cache, dollars, quarters+1, dimes, nickels, pennies)

    if _dimes > 0:

        make_change(total-10, cache, dollars, quarters, dimes+1, nickels, pennies)

    if _nickles > 0:

        make_change(total-5, cache, dollars, quarters, dimes, nickels+1, pennies)

    if _pennies > 0:
        make_change(total-1, cache, dollars, quarters, dimes, nickels, pennies+1)


if __name__ == "__main__":
    total = float(sys.argv[1])
    make_change(total * 100)
