import sys

def make_change(init_amount, amount, coins, purse, branches, count):
    if amount == 0:
        count[0] += 1
        return

    for c in coins:
        if amount - c >= 0:
            branch = "-".join([str(s) for s in sorted(purse + [c])])
            if branch not in branches:
                make_change(init_amount, amount - c, coins, purse + [c], branches, count)
            if amount < init_amount:
                branches.append(branch)

total = int(sys.stdin.readline().split()[0])
coins = [int(c) for c in sys.stdin.readline().split()]
count = [0]

make_change(total, total, coins, [], [], count)
print(count[0])
