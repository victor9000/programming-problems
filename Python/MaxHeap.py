import pprint

class MaxHeap:
    def __init__(self):
        self._values = [ None ]
        self._index = 0

    def print_tree(self):
        self.print_node(1, 0)

    def print_node(self, index, depth):
        if index <= self._index:
            print ''.rjust(4 * depth) + str(self._values[index])
            self.print_node(2 * index, depth + 1)
            self.print_node(2 * index + 1, depth + 1)

    def insert(self, value):
        self._index += 1
        self._values.append(value)
        self.percolate_up()

    def percolate_up(self):
        target = self._index
        parent = int(target / 2)

        while parent > 0 and self._values[target] > self._values[parent]:
            temp = self._values[parent]
            self._values[parent] = self._values[target]
            self._values[target] = temp

            target = parent
            parent = int(target / 2)

    def extract_max(self):
        self.delete_at(1)

    def delete_at(self, index):
        last = self._values.pop()
        self._index -= 1
        self._values[index] = last
        self.percolate_down(index)

    def percolate_down(self, index):
        target = index
        child_1 = 2 * target
        child_2 = 2 * target + 1

        while (
            (child_1 <= self._index and self._values[target] < self._values[child_1]) or
            (child_2 <= self._index and self._values[target] < self._values[child_2])
        ):
            if self._values[child_1] > self._values[child_2]:
                swap_value = self._values[child_1]
                swap_index = child_1
            else:
                swap_value = self._values[child_2]
                swap_index = child_2

            temp = self._values[target]
            self._values[target] = swap_value
            self._values[swap_index] = temp

            target = swap_index
            child_1 = 2 * target
            child_2 = 2 * target + 1


heap = MaxHeap()
heap.insert(10)
heap.insert(20)
heap.insert(5)
heap.insert(25)
heap.insert(45)
heap.insert(15)
heap.insert(30)
heap.print_tree()

heap.extract_max()
heap.print_tree()

heap.delete_at(2)
heap.print_tree()

heap.insert(50)
heap.insert(1)
heap.print_tree()
