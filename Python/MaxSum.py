import sys

nums = " ".join(sys.argv[1:])
a = [int(n) for n in nums.split()]
curr_sum = max_sum = nc_max = (sys.maxint-1)*-1

print a

for i in range(len(a)):
    curr_sum = max(a[i], curr_sum + a[i])

    if curr_sum > max_sum:
        max_sum = curr_sum

    nc_max = max(a[i], nc_max + a[i], nc_max)

print max_sum, nc_max
