
def maze_traverse(maze, cache=[], solution=[], row=0, col=0):
    key = str(row) + str(col)
    if key in cache:
        return
    else:
        cache.append(key)

    if row == len(maze) or col == len(maze[0]) or maze[row][col] == 'O':
        return None

    if maze[row][col] == 'f':
        solution += [(row, col)]
        return solution

    if maze[row][col] == ' ' or maze[row][col] == 's':
        temp = maze_traverse(maze, cache, solution + [(row, col)], row+1, col)
        if temp is None:
            temp = maze_traverse(maze, cache, solution + [(row, col)], row, col+1)

        return temp

maze = [
    ['s', ' ', ' ', ' ', ' '],
    [' ', ' ', ' ', 'O', ' '],
    [' ', ' ', ' ', 'O', ' '],
    [' ', ' ', ' ', 'O', ' '],
    [' ', ' ', ' ', 'O', ' '],
    [' ', ' ', ' ', 'O', 'f']
]
solution = maze_traverse(maze)

if solution is None:
    print 'No solution.'
else:
    for (r, c) in solution:
        maze[r][c] = '#'

    import pprint
    pprint.pprint(maze)
