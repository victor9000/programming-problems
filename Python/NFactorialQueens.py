import sys
import math

class Board:
    def __init__(self, board_tiles=[], board_rows=0, board_cols=0):
        self.tiles = board_tiles
        self.rows = board_rows
        self.columns = board_cols

def print_board(board):
    for r in board.tiles:
        print r

def legal_move(board, curr_row, curr_col):
    if board.tiles[curr_row][curr_col] != ".":
        return False

    # col up
    t_row = curr_row - 1
    while t_row >= 0:
        if board.tiles[t_row][curr_col] == 'Q':
            return False
        elif board.tiles[t_row][curr_col] == '#':
            break
        t_row -= 1

    # col down
    t_row = curr_row + 1
    while t_row < board.rows:
        if board.tiles[t_row][curr_col] == 'Q':
            return False
        elif board.tiles[t_row][curr_col] == '#':
            break
        t_row += 1

    # row left
    t_col = curr_col - 1
    while t_col >= 0:
        if board.tiles[curr_row][t_col] == 'Q':
            return False
        elif board.tiles[curr_row][t_col] == '#':
            break
        t_col -= 1

    # row right
    t_col = curr_col + 1
    while t_col < board.columns:
        if board.tiles[curr_row][t_col] == 'Q':
            return False
        elif board.tiles[curr_row][t_col] == '#':
            break
        t_col += 1

    # up & left
    t_row = curr_row + 1
    t_col = curr_col - 1
    while t_row < board.rows and t_col >= 0:
        if board.tiles[t_row][t_col] == 'Q':
            return False
        elif board.tiles[t_row][t_col] == '#':
            break
        t_row += 1
        t_col -= 1

    # up & right
    t_row = curr_row + 1
    t_col = curr_col + 1
    while t_row < board.rows and t_col < board.columns:
        if board.tiles[t_row][t_col] == 'Q':
            return False
        elif board.tiles[t_row][t_col] == '#':
            break
        t_row += 1
        t_col += 1

    # down & left
    t_row = curr_row - 1
    t_col = curr_col - 1
    while t_row >= 0 and t_col >= 0:
        if board.tiles[t_row][t_col] == 'Q':
            return False
        elif board.tiles[t_row][t_col] == '#':
            break
        t_row -= 1
        t_col -= 1

    # down & right
    t_row = curr_row - 1
    t_col = curr_col + 1
    while t_row >= 0 and t_col < board.columns:
        if board.tiles[t_row][t_col] == 'Q':
            return False
        elif board.tiles[t_row][t_col] == '#':
            break
        t_row -= 1
        t_col += 1

    return True

def get_marked_tiles(tiles, mark):
    open_tiles = [
        (r,c)
        for r,row in enumerate(tiles)
        for c,col in enumerate(row)
        if tiles[r][c] == mark
    ]
    return open_tiles

def get_available_tiles(tiles, curr_row, curr_col):
    available = [
        (r,c)
        for r,row in enumerate(tiles)
        for c,col in enumerate(row)
        if tiles[r][c] == '.' and r != curr_row and c != curr_col
    ]
    return available

def find_queens(board, available_tiles, cache, curr_row, curr_col, solutions):
    if legal_move(board, curr_row, curr_col):
        # mark solution
        board.tiles[curr_row][curr_col] = 'Q'
        solution_tiles = get_marked_tiles(board.tiles, 'Q')

        # calc key
        solution_tiles.sort(key=lambda tup: str(tup[0]) + str(tup[1]))
        key = "".join(map(lambda t: str(t[0]) + str(t[1]), solution_tiles))

        # check key
        if key not in cache:
            cache.append(key)
            solutions.append(clone_board(board))

        # reset tile
        board.tiles[curr_row][curr_col] = '.'

def clone_board(board):
    num_rows = board.rows
    num_cols = board.columns
    tiles = []

    for row in board.tiles:
        tiles.append(list(row))

    new_board = Board(tiles, num_rows, num_cols)

    return new_board

#------------------#
# start processing #
#------------------#

num_boards = int(sys.stdin.readline())

for i in range(num_boards):
    rc_str  = sys.stdin.readline().split()
    rows, cols = int(rc_str[0]), int(rc_str[1])
    tiles = []

    for j in range(rows):
        row_raw = sys.stdin.readline().strip()
        tiles.append([c for c in row_raw])

    solutions = [Board()]
    board = Board(tiles, rows, cols)
    count = 0
    num_queens = 1

    while len(solutions) > 0:
        next_solutions = []
        cache = []

        if num_queens == 1:
            for start_row in range(board.rows):
                for start_col in range(board.columns):
                    available = get_available_tiles(board.tiles, start_row, start_col)
                    find_queens(board, available, cache, start_row, start_col, next_solutions)
        else:
            for next_board in solutions:
                open_tiles = get_marked_tiles(next_board.tiles, '.')
                for tile in open_tiles:
                    start_row, start_col = tile[0], tile[1]
                    available = get_available_tiles(next_board.tiles, start_row, start_col)
                    find_queens(next_board, available, cache, start_row, start_col, next_solutions)

        num_queens += 1
        solutions = next_solutions
        count += len(solutions)

    print (count % (pow(10, 9) + 7))
