import sys
import math

def print_board(board):
    for r in board:
        print "|".join(r).replace('.', '_')
    print ''

def legal_move(board, total_rows, total_cols, curr_row, curr_col):
    for r in range(total_rows):
        if board[r][curr_col] == 'Q':
            return False
        elif board[r][curr_col] == '#':
            break

    for c in range(total_cols):
        if board[curr_row][c] == 'Q':
            return False
        elif board[curr_row][c] == '#':
            break

    # up & left
    t_row = curr_row
    t_col = curr_col
    while t_row < total_rows and t_col >= 0:
        if board[t_row][t_col] == 'Q':
            return False
        elif board[t_row][t_col] == '#':
            break
        t_row += 1
        t_col -= 1

    # up & right
    t_row = curr_row
    t_col = curr_col
    while t_row < total_rows and t_col < total_cols:
        if board[t_row][t_col] == 'Q':
            return False
        elif board[t_row][t_col] == '#':
            break
        t_row += 1
        t_col += 1

    return True

def find_queens(board, total_rows, total_cols, curr_row, count):
    if curr_row < 0:
        count[0] += 1
        print_board(board)
        return

    for c in range(total_cols):
        if legal_move(board, total_rows, total_cols, curr_row, c):
            board[curr_row][c] = 'Q'
            find_queens(board, total_rows, total_cols, curr_row - 1, count)
            board[curr_row][c] = '.'

if __name__ == "__main__":
    import sys
    dimension = int(sys.argv[1])
    rows, cols = int(dimension), int(dimension)
    board = []

    for j in range(rows):
        row_raw = "".rjust(cols, '.')
        board.append([c for c in row_raw])

    current_row = rows - 1
    count = [0]
    find_queens(board, rows, cols, current_row, count)
    print count[0]
