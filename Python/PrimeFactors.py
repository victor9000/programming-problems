import sys

def get_primes(max_num):
    candidates = range(2, max_num+1)
    candidates.reverse()
    primes = []

    while len(candidates) > 0:
        prime = candidates.pop()
        primes.append(prime)
        candidates = [c for c in candidates if c % prime != 0]

    return primes

def get_prime_factors(num, primes):
    factors = {}

    for i in primes:
        count=0

        while num % i == 0:
            count += 1
            num = num / i

        if count > 0:
            factors[i] = count

    return get_prime_factors

if __name__ == "__main__":
    num = int(sys.argv[1])
    primes = get_primes(num)
    factors = get_prime_factors(num, primes)
    print " x ".join(["{0}^{1}".format(k,v) for (k,v) in factors.iteritems()])
