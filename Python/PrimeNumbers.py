
nums = [x for x in range(100, 1, -1)]
primes = []

while len(nums) > 0:
    num = nums.pop()
    primes.append(num)
    nums = [x for x in nums if x % num != 0]

print primes
