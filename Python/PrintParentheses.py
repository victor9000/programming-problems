
def print_parens(num_parens, depth, num_open, num_close, parens=''):
    if depth == 2 * num_parens:
        print parens
        return

    if num_open == 0 and depth == 0 or num_open < num_parens:
        print_parens(num_parens, depth+1, num_open+1, num_close, parens+'(')

    if num_open == num_parens or num_close < num_open:
        print_parens(num_parens, depth+1, num_open, num_close+1, parens+')')


if __name__ == "__main__":
    import sys
    num = int(sys.argv[1])

    output = ''
    print_parens(num, 0, 0, 0)
    print output
