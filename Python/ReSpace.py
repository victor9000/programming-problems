
dictionary = [
    'a', 'as', 'car', 'catch', 'is', 'favor', 'for', 'hello',
    'lemon', 'no', 'pace', 'paragraph', 'sample',
    'spaces', 'text', 'this', 'with', 'wrote', 'zebra'
]

#
def score_phrase(string):
    tokens = string.split()
    score = 0

    for token in tokens:
        if token not in dictionary:
            score += len(token)

    return score

#
def respace(string):
    best_score = len(string) + 1
    prev_best = len(string) + 2
    best_parse = string

    while best_score < prev_best:
        prev_best = best_score
        string = best_parse

        for start in range(0, len(string)):
            for stop in range(start+1, len(string)+1):
                token = string[start:stop]
                phrase = string[:start] + ' ' + string[start:stop] + ' ' + string[stop:]
                score = score_phrase(phrase)

                if score < best_score:
                    best_score = score
                    best_parse = phrase

    return best_parse

#
nospace = 'mikewrotethissampletextparagraphwithnospacesasafavorforjim'
print respace(nospace)
