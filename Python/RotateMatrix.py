import pprint

class Pixel:
    def __init__(self, label, r, g, b):
        self._label = label
        self._r = r
        self._g = g
        self._b = b

    def __repr__(self):
        return "p" + str(self._label).zfill(2)

def rotate_matrix(image):
    num_layers = len(image[0])/2
    for layer in range(0, num_layers):
        start = layer
        stop = len(image[0])-layer-1
        for i in range(layer, stop):
            rev_i = len(image[0])-i-1
            temp = image[start][i]
            image[start][i] = image[rev_i][start]
            image[rev_i][start] = image[stop][rev_i]
            image[stop][rev_i] = image[i][stop]
            image[i][stop] = temp

pixel_image = [
    map(lambda x: Pixel(x,1,1,1), range(1, 6)),
    map(lambda x: Pixel(x,1,1,1), range(6, 11)),
    map(lambda x: Pixel(x,1,1,1), range(11,16)),
    map(lambda x: Pixel(x,1,1,1), range(16,21)),
    map(lambda x: Pixel(x,1,1,1), range(21,26))
]
pprint.pprint(pixel_image)
rotate_matrix(pixel_image)
pprint.pprint(pixel_image)
