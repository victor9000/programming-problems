import sys

def permute(elements, path, depth):
    if depth == 0:
        yield path
    else:
        for e in elements:
            next_elements = elements ^ set([e])
            for p in permute(next_elements, path + str(e), depth-1):
                yield p

num_items = int(sys.argv[1])
set_items = set(range(1, num_items+1))
generator = permute(set_items, '', num_items)

for item in generator:
    print item
