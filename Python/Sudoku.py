#!/bin/python
import sys
from random import shuffle

def legal_move(num, grid, curr_row, curr_col):
    if grid[curr_row][curr_col] != 0:
        return False

    # check rows and columns
    for index in range(9):
        if grid[curr_row][index] == num:
            return False
        if grid[index][curr_col] == num:
            return False

    quad_row = int(curr_row / 3) * 3
    quad_col = int(curr_col / 3) * 3

    for row in range(quad_row, quad_row + 3):
        for col in range(quad_col, quad_col + 3):
            if grid[row][col] == num:
                return False

    return True

def get_next_coordinates(grid, row, col):
    for t_col in range(col, 9):
        if grid[row][t_col] == 0:
            return row, t_col

    for t_row in range(row+1, 9):
        for t_col in range(9):
            if grid[t_row][t_col] == 0:
                return t_row, t_col

    return -1, -1

def get_num_moves(grid):
    num = 0

    for r in range(9):
        for c in range(9):
            if grid[r][c] == 0:
                num += 1

    return num

def print_grid(grid):
    for r in range(9):
        output = ""
        for c in range(9):
            output += str(grid[r][c]) + " "
        print output

def sudoku_solve(grid, curr_row, curr_col, num_moves, count):
    if num_moves == 0:
        print_grid(grid)
        return True

    if count[0] > 1000000:
        return False

    available_nums = list(
        set(range(1,10)) -
        set([row[curr_col] for row in grid]) -
        set(grid[curr_row])
    )
    shuffle(available_nums)

    for num in available_nums:
        count[0] += 1
        if legal_move(num, grid, curr_row, curr_col):
            grid[curr_row][curr_col] = num
            next_row, next_col = get_next_coordinates(grid, curr_row, curr_col)
            solved = sudoku_solve(grid, next_row, next_col, num_moves-1, count)
            grid[curr_row][curr_col] = 0

            if solved == True:
                return True

    return False

index = -1
boards = {}
num_boards = input()
input_lines = sys.stdin.readlines()
line_count = 0

# get boards
for line in input_lines:
    if line_count % 9 == 0:
        index += 1
        boards[index] = []

    boards[index].append([int(k) for k in line.split()])
    line_count += 1

# solve boards
index = 0
while index < num_boards:
    count = [0]
    num_moves = get_num_moves(boards[index])
    next_row, next_col = get_next_coordinates(boards[index], 0, 0)
    solved = sudoku_solve(boards[index], next_row, next_col, num_moves, count)

    if solved == True:
        index += 1
