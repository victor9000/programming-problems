
def take_steps(num_steps):
    steps = [-1] * (num_steps + 1)
    steps[0] = 0
    steps[1] = 1 + steps[0]
    steps[2] = 1 + steps[1] + steps[0]
    steps[3] = 1 + steps[2] + steps[1] + steps[0]

    if num_steps < 4:
        return steps[num_steps]

    for i in range(4, num_steps+1):
        steps[i] = steps[i-1] + steps[i-2] + steps[i-3]

    return steps[-1]

print take_steps(4)
