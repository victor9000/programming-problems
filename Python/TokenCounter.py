import re

class TokenCounter():
    def __init__(self, blob):
        self._tokens={}
        for token in re.split('\W+', blob):
            token = token.lower().strip()
            if token is None or token is '':
                continue

            if token in self._tokens:
                self._tokens[token] += 1
            else:
                self._tokens[token] = 1

    def get_count(self, token):
        return self._tokens[token]

book = "This is quite a small book.  This book is only two sentences long."
counter = TokenCounter(book)

import pprint
pprint.pprint(counter.get_count('book'))
