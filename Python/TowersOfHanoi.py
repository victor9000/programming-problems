
def towers(level, source, buff=[], destination=[]):
    if level == 0:
        return

    towers(level-1, source, destination, buff)

    item = source.pop()
    destination.append(item)

    print source, buff, destination

    towers(level-1, buff, source, destination)


start = range(1,6)
start.reverse()
stop = []

print start
print stop
towers(len(start), start, [], stop)
print start
print stop
