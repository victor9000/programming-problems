
class Node:
    def __init__(self, data=None, node1=None, node2=None):
        self._data = data
        self._node1 = node1
        self._node2 = node2

    def print_tree(self, depth=0):
        print (' ' * (depth * 4)) + str(self._data)

        if self._node1 is not None:
            self._node1.print_tree(depth+1)
        if self._node2 is not None:
            self._node2.print_tree(depth+1)

    def print_list(self):
        print self._data
        if self._node2 is not None:
            self._node2.print_list()

def flatten(node):
    if node is None:
        return (None, None)

    left_head, left_tail = flatten(node._node1)
    if left_tail is not None:
        left_tail._node2 = node
        node._node1 = left_tail

    right_head, right_tail = flatten(node._node2)
    if right_head is not None:
        node._node2 = right_head
        right_head._node1 = node

    new_head = left_head if left_head is not None else node
    new_tail = right_tail if right_tail is not None else node

    return (new_head, new_tail)

# make tree
nodeA = Node('A')
nodeC = Node('C')
nodeB = Node('B', nodeA, nodeC)
nodeO = Node('O')
nodeZ = Node('Z')
nodeR = Node('R', nodeO, nodeZ)
nodeE = Node('E')
nodeM = Node('M', nodeE, nodeR)
nodeD = Node('D', nodeB, nodeM)

nodeD.print_tree()
head, tail = flatten(nodeD)
head.print_list()
